# Gráficas RC

Repositorio de gráficas producidas por [Raíces Casablanca](https://gitlab.com/raices.cba) para publicaciones y otros contenidos.




## Licencia

Contenidos bajo licencia [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es_ES).

El texto de la licencia se encuentra en [License](https://gitlab.com/raices.cba/graficas-rc/-/blob/master/LICENSE).


